var express = require("express");
var app     = express();
var path    = require("path");

// Headers
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/View/index.html'));
});

app.get('/shopping-list',function(req,res){
    res.sendFile(path.join(__dirname+'/View/shopping-list.html'));
});

app.get("/list",function(req,res){
    res.json([ {"task":"get milk","who":"Scott","dueDate":"2013-05-19","done":false},
        {"task":"get broccoli","who":"Elisabeth","dueDate":"2013-05-21","done":false},
        {"task":"get garlic","who":"Trish","dueDate":"2013-05-30","done":false},
        {"task":"get eggs","who":"Josh","dueDate":"2013-05-15","done":true} ]);
});

app.listen(3000);
console.log("Running at Port 3000");